$(document).ready(function() {
	
	//фильтр скидок на главной
	$('#filter .filter-item').on('click', function(event) {
		var typeToSort = $(this).attr('id'),
			item = $('#items-holder .item');
		$('#items-holder .item').hide();
		item.filter('[data-type=' + typeToSort + ']').fadeIn(300);
		event.preventDefault();
	});

	//вкладки этаже на главной
	$('#scheme .tabs .item').on('click', function(event) {
		var scheme = $(this).parents('#scheme'),
			tabs = $(this).parents('#scheme').find('.tabs .item'),
			curTab =  $(this).data('id'),
			holderTab = $(this).parents('#scheme').find('.scheme');
		holderTab.hide();
		$('#'+ curTab).fadeIn(700);
		tabs.removeClass('active');
		$(this).addClass('active');
		event.preventDefault();
	});

	//открывание меню
    $('.burger').on('click', function(){
    	var menu = $('.b-header .menu'),
    		logo = $('.b-header .logo');
    	$(this).toggleClass('active');
    	menu.toggleClass('show');
    	logo.toggleClass('active');
    	return false;
    });

	// всплывающие блоки на схеме
	$('.b-scheme .popup').hover(function() {
		var logo = $(this).data('logo'),
			name = $(this).data('name'),
			place = $(this).data('place'),
			// itemWidth = $(this).outerWidth(),
			// itemHeight = $(this).height(),
			left = Math.trunc($(this).offset().left),
			top = Math.trunc($(this).offset().top),
			popup = '<div class="popup-holder" style="top:' + top + 'px; left: ' + left + 'px;"><div class="logotype"><img src="' + logo + '"></div><div class="name">' + name + '</div><div class="place">' + place + '</div></div>';
		$(this).parents('.scheme').append(popup);
		//console.log(itemWidth);
	}, function() {
		$(this).parents('.scheme').find('.popup-holder').remove();
	});


	//коллапсы
	$('.b-collapse .collapse-header').on('click', function() {
		var header = $(this),
			content = $(this).parents('.b-collapse').find('.collapse-content');
		header.toggleClass('open');
		if ( header.hasClass('open') ) {
			content.slideDown(300);
		} else {
			content.slideUp(300);
		}
	});
});